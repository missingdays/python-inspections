import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.buildFeatures.pullRequests
import jetbrains.buildServer.configs.kotlin.v2019_2.buildSteps.script
import jetbrains.buildServer.configs.kotlin.v2019_2.projectFeatures.spaceConnection
import jetbrains.buildServer.configs.kotlin.v2019_2.triggers.vcs

/*
The settings script is an entry point for defining a TeamCity
project hierarchy. The script should contain a single call to the
project() function with a Project instance or an init function as
an argument.

VcsRoots, BuildTypes, Templates, and subprojects can be
registered inside the project using the vcsRoot(), buildType(),
template(), and subProject() methods respectively.

To debug settings scripts in command-line, run the

    mvnDebug org.jetbrains.teamcity:teamcity-configs-maven-plugin:generate

command and attach your debugger to the port 8000.

To debug in IntelliJ Idea, open the 'Maven Projects' tool window (View
-> Tool Windows -> Maven Projects), find the generate task node
(Plugins -> teamcity-configs -> teamcity-configs:generate), the
'Debug' option is available in the context menu for the task.
*/

version = "2021.1"

project {

    buildType(Build)

    features {
        spaceConnection {
            id = "PROJECT_EXT_2"
            displayName = "JetBrains Space"
            serverUrl = "https://rocketmuscles.jetbrains.space/"
            clientId = "273e36d6-5823-45b5-a797-c85f66e77289"
            clientSecret = "credentialsJSON:fcfdc1be-b6a4-4d43-8dfd-b74e7b301995"
        }
    }
}

object Build : BuildType({
    name = "Build"

    vcs {
        root(DslContext.settingsRoot)
    }

    steps {
        script {
            scriptContent = """
                echo "##teamcity[inspectionType id='TEST_INSPECTION' name='Test inspection' description='This is a test inspection' category='Test inspections']"
                echo "##teamcity[inspection typeId='TEST_INSPECTION' message='Test inspection message' file='main.py' line='3']"
            """.trimIndent()
        }
    }

    triggers {
        vcs {
        }
    }

    features {
        pullRequests {
            vcsRootExtId = "${DslContext.settingsRoot.id}"
            param("filterTargetBranch", "refs/heads/main")
            param("providerType", "jetbrainsSpace")
        }
    }
})
